var assert = require('assert');
var Queue = require('../src/js/util/queue');

suite('Queue class', function() {
  test('New Queue objects can be created', function() {
    assert(new Queue());
  });

  test('An item can be enqueued', function() {
    var q = new Queue();

    assert.equal(1, q.enqueue('foo'));
    assert.equal(2, q.enqueue('bar'));
    assert.equal(3, q.enqueue('baz'));

    assert.equal(3, q.items().length);
  });

  test('Constructor takes array or arguments', function() {
    var q1 = new Queue('foo', 'bar', 'baz');
    var q2 = new Queue(['foo', 'baz']);

    assert.equal(q1.head(), q2.head());
    assert.equal(q1.tail(), q2.tail());
  });

  test('Items can be dequeued', function() {
    var q = new Queue(['foo', 'bar', 'baz']);

    assert.equal('foo', q.dequeue());
    assert.equal('bar', q.dequeue());
    assert.equal('baz', q.dequeue());

    assert.equal(0, q.items().length);
  })

  test('Head/Tail work and are non-destructive', function() {
    var q = new Queue(['foo', 'bar']);

    assert.equal('foo', q.head());
    assert.equal('foo', q.head());
    assert.equal('bar', q.tail());
    assert.equal('bar', q.tail());

    assert.equal(2, q.items().length);
  });

});