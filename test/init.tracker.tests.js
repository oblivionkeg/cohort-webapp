var assert = require('assert');
var Combatant = require('../src/js/initiative/combatant');
var Tracker = require('../src/js/initiative/tracker');

suite('Initiative combatant', function() {
  test('has an initiative accessor', function() {
    var c = new Combatant();

    /** has a default value that implies not-in-combat */
    assert(c.initiative);
    assert.equal(null, c.initiative()); // should be ignored in combat

    /** set initiative */
    c.initiative(10); // chainable
    assert.equal(10, c.initiative());

    c.initiative(15);
    assert.equal(15, c.initiative());
  })
});

suite('Initiative tracker', function() {
  test('initialization', function() {
    assert(new Tracker());
  });

  test('can take entities', function() {
    var t = new Tracker();

    t.add_entity('foo');
    t.add_entity('bar');
    t.add_entity('baz');

    assert.equal(3, t.entities().length);
  });

  test('can sort entities', function() {
    var t = new Tracker();

    // to sort, the tracker compares the result of the entities' value methods
    var Entity = function(val) {
      this.val = val;
      this.value = function() { return this.val; }
    };

    t.add_entity(new Entity(15));
    t.add_entity(new Entity(50));
    t.add_entity(new Entity(10));
    t.add_entity(new Entity(20));

    t.sort();

    // Tracker sorts highest to lowest
    assert.equal(50, t.queue().head().value());
    assert.equal(10, t.queue().tail().value());
  });
})