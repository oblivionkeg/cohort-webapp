module.exports = (function() {
  'use strict';

  var Queue = function(array) {
    if (! (array instanceof Array)) { array = Array.prototype.slice.call(arguments); }
    this._data = array;
    return this;
  };

  /** Add a new entry to the tail of the queue */
  Queue.prototype.enqueue = function(obj) {
    this._data.push(obj);
    return this._data.length;
  };

  /** Remove the head of the queue and return it */
  Queue.prototype.dequeue = function() {
    return this._data.splice(0, 1)[0];
  };

  /** Non-destructive accessor to the first item the queue */
  Queue.prototype.head = function() {
    return this._data[0];
  };

  /** Non-destructive accessor to the last item in the queue */
  Queue.prototype.tail = function(first_argument) {
    return this._data[this._data.length - 1];
  };

  /** Get a copy of the list of items in the queue */
  Queue.prototype.items = function() {
    return this._data.slice(0);
  }

  return Queue;
}());