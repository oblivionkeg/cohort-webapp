module.exports = (function() {
  'use strict';

  var Queue = require('../util/queue');

  var InitiativeTracker = function() {
    this._entities = [];
    this._queue = new Queue();

    return this;
  };

  /** add a player/monster/npc to the combat */
  InitiativeTracker.prototype.add_entity = function(entity) {
    this._entities.push(entity);
    return this;
  };

  /** add multiple entities to combat */
  InitiativeTracker.prototype.add_entities = function(entities) {
    entities.forEach(this.add_entity.bind(this));
    return this;
  };

  /** Get a shallow copy of those being tracked */
  InitiativeTracker.prototype.entities = function() {
    return this._entities.slice(0);
  }

  /** sort all entities with valid initiative values into queue */
  InitiativeTracker.prototype.sort = function() {
    this._entities.sort(function(a, b) {
      return b.initiative() - a.initiative(); // sort in descending order
    });
    this._queue = new Queue(this._entities);
    return this;
  };

  InitiativeTracker.prototype.queue = function() {
    return this._queue;
  };

  return InitiativeTracker;
}());
