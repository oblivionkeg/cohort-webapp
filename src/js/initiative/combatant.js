/**
 * Combat representative of a player or NPC
 */
module.exports = (function() {
  'use strict';

  /**
   * At its simplest, a combatant has an identifier, and a
   * value to sort by (their initiative)
   * @param {[type]} identifier - unique identifier
   */
  var Combatant = function(id) {
    this._id = id;

    return this;
  };

  Combatant.prototype._initiative = null;

  /** Chainable get/setter for initiative roll */
  Combatant.prototype.initiative = function(init_mod) {
    if (init_mod !== undefined) {
      this._initiative = init_mod;
      return this;
    }
    return this._initiative;
  };

  /** Deprecated */
  Combatant.prototype.roll_initiative = function() {
    return Math.round(Math.random() * 20) + (this._init_mod || 0);
  };

  return Combatant;
}());
