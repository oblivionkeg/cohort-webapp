/** Cohort gulpfile */

let gulp = require('gulp')
let jade = require('gulp-jade')
let sass = require('gulp-sass')
let gutil = require('gulp-util')
let plumber = require('gulp-plumber')

let exec = require('child_process').exec

const build_dir = './app/';
const source_dir = './src/';

const css = source_dir + 'scss/*.scss';
const templates = source_dir + 'tmpl/**/*.jade';
const js = source_dir + 'js/**/*.js';

var onError = function(err) {
  let plugin=err.plugin
    , name=err.name 
    , message=err.message
    , log=gutil.log

  let fileName = err.fileName
    , lineNumber = err.lineNumber;

  log('')
  log(plugin +': '+ name)
  log(message)
  if (fileName) { log('File: ' + fileName) }
  if (lineNumber) { log('Line: ' + lineNumber) }
  log('')
} 

gulp.task('styles', function(callback) {
  const dest=build_dir+'css/'
      , fontsome='node_modules/font-awesome/';

  gulp.src(css)
    .pipe(plumber({errorHandler: onError}))
    .pipe(sass())
    .pipe(gulp.dest(dest))

  // pipe font-awesome
  gulp.src(fontsome + 'css/font-awesome.min.css')
    .pipe(gulp.dest(dest+'lib'))
  gulp.src(fontsome + 'fonts/fontawesome-webfont.*')
    .pipe(gulp.dest(dest+'fonts'))

  callback()
})

gulp.task('scripts', function(callback) {
  let log = gutil.log

  exec(
    (
      './node_modules/.bin/webpack '+
        source_dir +'js/main.js '+
        build_dir +'js/cohort.js'
    ),
    err => {
      log(err)
      callback(err)
    }
  )
})

gulp.task('templates', function(callback) {
  var opts={pretty:true}
    , dest=build_dir;

  gulp.src(templates)
    .pipe(plumber({errorHandler: onError}))
    .pipe(jade(opts))
    .pipe(gulp.dest(dest))
  callback()
})

gulp.task('watch', function(callback) {
  var log=gutil.log;

  var error_reporter = function(err) {
    log('We have a problem: ' + err)
  }

  gulp.watch(css, ['styles'])
    .on('error', error_reporter)

  gulp.watch(templates, ['templates'])
    .on('error', error_reporter)

  gulp.watch(js, ['scripts'])
    .on('error', error_reporter)

  callback()
})

gulp.task('build', ['watch', 'templates', 'scripts', 'styles'])

